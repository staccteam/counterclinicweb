const BASE_API_URL  = "https://reqres.in";
const LOGIN_URL = "/login";
const RECEPTION_DASHBOARD_URL = "reception/dashboard";
const DOCTOR_DASHBOARD_URL = "/doctor/dashboard";
const ADMIN_DASHBOARD_URL = "/admin/dashboard";
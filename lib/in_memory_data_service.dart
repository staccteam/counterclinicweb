import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:counter_clinic_web/service_urls.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';

class InMemoryDataService implements MockClient {
  @override
  void close() {
    // TODO: implement close
  }

  @override
  Future<Response> delete(url, {Map<String, String> headers}) {
    // TODO: implement delete
  }

  @override
  Future<Response> get(url, {Map<String, String> headers}) {
    // TODO: implement get
  }

  @override
  Future<Response> head(url, {Map<String, String> headers}) {
    // TODO: implement head
  }

  @override
  Future<Response> patch(url, {Map<String, String> headers, body, Encoding encoding}) {
    // TODO: implement patch
  }

  @override
  Future<Response> post(url, {Map<String, String> headers, body, Encoding encoding}) async {
    switch (url)
    {
      case LOGIN_URL:
        String accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJ1c2VybmFtZSI6InZzbGFsYSJ9.AGQGk79S-CTGhY_BoyjEmetRLsQTWwq7yE10kpIrEeE";
        return Response(accessToken, 200, headers: {'content-type':'application/json'});
      break;
      default:
        throw 'No Post Url Defined for: $url';
      break;  
    }
  }

  @override
  Future<Response> put(url, {Map<String, String> headers, body, Encoding encoding}) {
    // TODO: implement put
  }

  @override
  Future<String> read(url, {Map<String, String> headers}) {
    // TODO: implement read
  }

  @override
  Future<Uint8List> readBytes(url, {Map<String, String> headers}) {
    // TODO: implement readBytes
  }

  @override
  Future<StreamedResponse> send(BaseRequest request) {
    // TODO: implement send
  }
  
 
}
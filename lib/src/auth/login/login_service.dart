import 'dart:async';
import 'dart:convert';

import 'package:angular_router/angular_router.dart';
import 'package:counter_clinic_web/constants.dart';
import 'package:counter_clinic_web/src/model/User.dart';
import 'package:counter_clinic_web/src/storage_manager.dart';
import 'package:http/browser_client.dart';
import 'package:http/http.dart';

const USER_AUTH_TEST_DATA = {
  "userId": "RANDOMID123",
  "firstName": "Varun",
  "lastName": "Shrivastava",
  "email" : "varunshrivastava007@gmail.com",
  "username":  "vslala",
  "roles": [
      {
      "name" : "RECEPTION",
      "permissions": [
        {
          "value": "CAN_CREATE_NEW_USER"
        },
        {
          "value": "CAN_ASSIGN_ROLES_TO_USER"
        },
        {
          "value": "CAN_DELETE_USER"
        },
        {
          "value": "CAN_REMOVE_ROLES_OF_USER"
        }
      ]
    }
  ]
};


class LoginService {
  static final _headers = {'Content-Type': 'application/json'};
  static final _loginUrl = '$BASE_API_URL/api/login';
  Location _location;
  final BrowserClient _http;


  LoginService(this._http, this._location) ;

  Future<String> loginUser(String username, String password)  async  {
    String tokenJson;
    try {
      Future<Response> tokens = _http.post(_loginUrl, 
      body: jsonEncode(
        {'email':username, 'password':password})
      );
    
      Response response  = await tokens;

      String tokenJson  =   response.body;

      print(tokenJson);
      _location.go(RECEPTION_DASHBOARD_URL);

    } catch (e) {
       print(e.toString());
    }
    

    return tokenJson;

  } 

  Future<User> fetchUserAuthData() async {
    String token = await getFromLocalStorage("authToken");

    //TODO: Call auth service to fetch user auth data
    // Write code to fetch auth data from service here...

    return User.fromJson(USER_AUTH_TEST_DATA);
  }

}
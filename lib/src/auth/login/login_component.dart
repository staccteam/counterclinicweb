import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:counter_clinic_web/src/auth/login/login_service.dart';
import 'package:counter_clinic_web/constants.dart';
import 'package:counter_clinic_web/src/model/User.dart';
import 'package:counter_clinic_web/src/storage_manager.dart';


@Component(
  selector: 'login-component',
  templateUrl: 'login_component.html',
  styleUrls: const [
    'login_component.css',
    'package:angular_components/css/mdc_web/card/mdc-card.scss.css'
  ],
  directives:  [
    MaterialInputComponent,
    MaterialButtonComponent,
    materialInputDirectives,
    formDirectives,
  ],
  providers: [ClassProvider(LoginService)]
)
class LoginComponent {

  final LoginService _loginService;
  final Location _location;
  final Router _router;

  String title = "User Login";
  String username = "";
  String password = "";
  String token = "";

  LoginComponent(this._loginService, this._location, this._router);

  onSubmit() async {
    print(username + ":" + password);
    // TODO: use service to fetch data  in production
    // token = await _loginService.loginUser(username, password);
    token = "some_token";
    if (token == null || token.isEmpty)  {
      _location.go(LOGIN_URL);
      return;
    }

    await saveToLocalStorage("authToken", token);

    User user = await _loginService.fetchUserAuthData();
    
    var roles = [];

    print("printing roles");

    for (var r in user.roles) {
      print(r.name);
      roles.add(r.name);
    }

    if (roles.contains("DOCTOR"))
      _location.go(DOCTOR_DASHBOARD_URL);
    else if (roles.contains("RECEPTION"))
      _router.navigate(RECEPTION_DASHBOARD_URL);
    else if (roles.contains("ADMIN"))
      _location.go(ADMIN_DASHBOARD_URL);
      
  }
}

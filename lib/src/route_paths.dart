import 'package:angular_router/angular_router.dart';
import 'package:counter_clinic_web/constants.dart';

class RoutePaths {
  static final login =  RoutePath(path: LOGIN_URL);
  static final receptionDashboard = RoutePath(path: RECEPTION_DASHBOARD_URL);
}
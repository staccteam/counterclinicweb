import 'package:angular/angular.dart';
import 'package:angular_components/material_tab/material_tab.dart';
import 'package:angular_components/material_tab/material_tab_panel.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:counter_clinic_web/src/model/Clinic.dart';
import 'package:counter_clinic_web/src/model/Doctor.dart';
import 'package:counter_clinic_web/src/reception_dashboard/assign_doctor_clinic_component.dart';
import 'package:counter_clinic_web/src/reception_dashboard/book_appointment_component.dart';
import 'package:counter_clinic_web/src/reception_dashboard/dashboard_service.dart';
import 'package:counter_clinic_web/src/reception_dashboard/doctor_queue_info_component.dart';
import 'package:counter_clinic_web/src/reception_dashboard/show_all_appointment_component.dart';

@Component(
  selector: 'reception-dashboard',
  styleUrls:  [
    'package:angular_components/css/mdc_web/card/mdc-card.scss.css'
  ],
  templateUrl: 'reception_dashboard_component.html',
  directives: [
    coreDirectives,
    MaterialInputComponent,
    MaterialButtonComponent,
    materialInputDirectives,
    formDirectives,
    MaterialTabComponent,            
    MaterialTabPanelComponent,
    MaterialDropdownSelectComponent,
    MaterialSelectComponent,
    BookAppointmentComponent,
    AssignDoctorClinicComponent,
    ShowAllAppointmentComponent,
    DoctorQueueInfoComponent,
  ],
  providers: [
    ClassProvider(DashboardService)
  ],
)
class ReceptionDashboard implements OnInit {

  DashboardService _dashboardService;

  ReceptionDashboard(this._dashboardService);

  var doctors = [];
  var clinics = [];
  var appointments = [];
  var queueStats = [];

  String patientName = "";

  @override
  void ngOnInit() async {
    var doctorsFuture = _dashboardService.getDoctors();
    var clinicsFuture = _dashboardService.getClinics();
    var appointmentFuture = _dashboardService.getAppointments();
    var queueStatsFuture = _dashboardService.getDoctorQueueStats();

    doctors = await doctorsFuture;
    clinics = await clinicsFuture;
    appointments = await appointmentFuture;
    queueStats = await queueStatsFuture;
  }
}

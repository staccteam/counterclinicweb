import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:counter_clinic_web/src/model/Doctor.dart';
import 'package:counter_clinic_web/src/reception_dashboard/dashboard_service.dart';

@Component(
  selector: 'book-appointment',
  templateUrl: 'book_appointment_component.html',
  directives: [
    coreDirectives,
    formDirectives
  ],
  providers:[ClassProvider(DashboardService)]
)
class BookAppointmentComponent {
  var name = 'Angular';
  final DashboardService _dashboardService;

  @Input()
  var doctors = [];
  @Input()
  var clinics = [];

  var selectedDoctorId = "Select Doctor...";
  var selectedClinicId = "Select Clinic...";

  BookAppointmentComponent(this._dashboardService);

  void bookAppointment() {
    print("Selected Doctor: $selectedDoctorId \nSelected Clinic: $selectedClinicId");
  }
}

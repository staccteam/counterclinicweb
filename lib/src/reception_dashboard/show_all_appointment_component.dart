import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';

@Component(
  selector: 'all-appointments',
  templateUrl: 'show_all_appointment_component.html',
  directives: [
    coreDirectives,
    formDirectives
  ],
)
class ShowAllAppointmentComponent {
  @Input()
  var appointments;

  void deleteAppointment(String appointmentId) {
    print("Appointment to delete: $appointmentId");
  }
}

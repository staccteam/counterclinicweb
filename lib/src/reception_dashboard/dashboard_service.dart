import 'dart:async';

import 'package:counter_clinic_web/src/model/Appointment.dart';
import 'package:counter_clinic_web/src/model/Clinic.dart';
import 'package:counter_clinic_web/src/model/Doctor.dart';
import 'package:counter_clinic_web/src/model/QueueStatus.dart';

var doctors = [
      Doctor.NewInstance("1", "Varun Shrivastava"),
      Doctor.NewInstance("2", "Vaibhav Shrivastava"),
      Doctor.NewInstance("3", "Rajesh Shrivastava"),
      Doctor.NewInstance("4", "Kavita Shrivastava")
    ];

var clinics = [
      Clinic.NewInstance("1", "ROOM-N1", "Autopsy"),
      Clinic.NewInstance("2", "X-Ray", "X-Ray"),
      Clinic.NewInstance("3", "ICU", "Intense Care Unit")
];

var queueStats = [
      QueueStatus(
        id: "1",
        doctorName: "Rajesh Shrivastava",
        clinicNumber: "ROOM-A01",
        queueSize: "5",
        currPatientInQueue: "1",
        doctorStatus: "In Progress"
      ),
      QueueStatus(
        id: "2",
        doctorName: "Kavita Shrivastava",
        clinicNumber: "ROOM-A01",
        queueSize: "8",
        currPatientInQueue: "3",
        doctorStatus: "In Progress"
      ),
];

var appointments = [
      Appointment(
        appointmentId: "1", 
        appointmentNumber: "5",
        appointedDoctor: "Priyanka Yadav",
        clinicNumber: "ROOM 456",
        patientName: "Angwati Nanabhai",
        qrCodeImagePath: "http://localhost:8080/application/images/qr/qrcode__1541600144075.png",
        timeOfAppointment: "2018-11-07 14:15:44.0"
      )
];

class DashboardService {

  Future<List<Doctor>> getDoctors() async {
    //  TODO: Call service to retreive doctors
    return doctors;
  }

  Future<List<Clinic>> getClinics() async {
    //  TODO: Call service to retreive clinics
    return clinics;
  }

  Future<List<Appointment>> getAppointments() async {
    //  TODO: Call service to retreive appointments
    return appointments;
  }

  Future<List<QueueStatus>> getDoctorQueueStats()  async {
    //  TODO: Call service to retreive queue stats
    return queueStats;
  }
}
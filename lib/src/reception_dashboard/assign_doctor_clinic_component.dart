import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';

@Component(
  selector: 'assign-doctor-clinic',
  templateUrl: 'assign_doctor_clinic_component.html',
  directives: [
    coreDirectives,
    formDirectives
  ],
)
class AssignDoctorClinicComponent {
  @Input()
  var doctors = [];

  @Input()
  var clinics = [];

  var selectedDoctorId = "Select Doctor...";
  var selectedClinicId = "Select Clinic...";

  void assignDoctorToClinic() {
    print("Assigned Doctor Id: $selectedDoctorId \nAssigned Clinic Id: $selectedClinicId");
  }
}

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';

@Component(
  selector: 'doctor-queue-info',
  templateUrl: 'doctor_queue_info_component.html',
  directives: [
    coreDirectives,
    formDirectives,
  ],
)
class DoctorQueueInfoComponent {
  @Input()
  var queueStats = [];

  resetQueue(String id) {
    print("Queue to reset: $id");
  }
}

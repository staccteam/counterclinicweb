import 'dart:async';
import 'dart:html';

saveToLocalStorage(String key, String value) async {
  window.localStorage[key] = value;
}

Future<String> getFromLocalStorage(String key) async {
  return window.localStorage[key];
}
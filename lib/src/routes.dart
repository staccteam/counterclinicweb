import 'package:angular_router/angular_router.dart';
import 'package:counter_clinic_web/src/route_paths.dart';
import 'auth/login/login_component.template.dart' as login_template;
import 'reception_dashboard/reception_dashboard_component.template.dart' as reception_dashboard_template;

export 'route_paths.dart';

class Routes {
  static final login = RouteDefinition(
    routePath: RoutePaths.login,
    component: login_template.LoginComponentNgFactory,
  );

  static final receptionDashboard = RouteDefinition(
    routePath: RoutePaths.receptionDashboard,
    component: reception_dashboard_template.ReceptionDashboardNgFactory
  );

  static final all  = <RouteDefinition>[
    login,
    receptionDashboard
  ];
}
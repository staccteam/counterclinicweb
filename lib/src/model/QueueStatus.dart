class QueueStatus {
  String id;
  String doctorName;
  String clinicNumber;
  String queueSize;
  String currPatientInQueue;
  String doctorStatus;

  QueueStatus({this.id,this.doctorName,this.clinicNumber,this.queueSize,this.currPatientInQueue,this.doctorStatus});

  QueueStatus.fromJson(Map<String, dynamic> _map) {
    this.id  = _map["id"];
    this.doctorName = _map["doctorName;"];
    this.clinicNumber = _map["clinicNumber"];
    this.queueSize = _map["queueSize"];
    this.currPatientInQueue = _map["currPatientInQueue"];
    this.doctorStatus = _map["doctorStatus"];
  }
}
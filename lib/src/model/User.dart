import 'package:counter_clinic_web/src/model/Role.dart';

class User {
  String userId;
  String firstName;
  String lastName;
  String email;
  String mobile;
  var roles = List<Role>();

  User.fromJson(Map<String, dynamic> _map) {
    this.userId  = _map["userId"];
    this.firstName = _map["firstName"];
    this.lastName = _map["lastName"];
    this.email = _map["email"];
    this.mobile = _map["mobile"];

    for (var role in _map["roles"]) {
      this.roles.add(Role.fromJson(role));
    }
  }
}
class Clinic {

  Clinic(){}

  Clinic.NewInstance(String clinicId, String clinicNumber, String clinicName) {
    this.clinicId =  clinicId;
    this.clinicNumber = clinicNumber;
    this.clinicName = clinicName;
  }

  String clinicId;
  String clinicNumber;
  String clinicName;
}
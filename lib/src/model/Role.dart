import 'package:counter_clinic_web/src/model/Permission.dart';

class Role {
  String name;
  var permissions = List<Permission>();

  Role.fromJson(Map<String, dynamic> _map){
    this.name = _map["name"];
    var permissions = _map["permissions"];
    assert (permissions is List);

    for(var p in permissions) {
      this.permissions.add(Permission.fromJson(p));
    }
  }
}
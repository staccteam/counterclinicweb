class Appointment {
  String appointmentId;
  String appointmentNumber;
  String patientName;
  String appointedDoctor;
  String clinicNumber;
  String timeOfAppointment;
  String qrCodeImagePath;

  Appointment(
    {
    this.appointmentId, 
    this.appointmentNumber, 
    this.patientName, 
    this.appointedDoctor, 
    this.clinicNumber,
    this.timeOfAppointment,
    this.qrCodeImagePath
    }
  ){}

  Appointment.fromJson(Map<String, dynamic> _map) {
    this.appointmentId = _map["appointmentId"];
    this.appointmentNumber = _map["appointmentNumber"];
    this.patientName  = _map["patientName"];
    this.appointedDoctor  = _map["appointedDoctor"];
    this.clinicNumber  = _map["clinicNumber"];
    this.timeOfAppointment = _map["timeOfAppointment"];
    this.qrCodeImagePath = _map["qrCodeImagePath"];
  }
}
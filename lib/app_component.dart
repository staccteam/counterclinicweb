import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:angular_router/angular_router.dart';
import 'package:counter_clinic_web/src/auth/login/login_service.dart';
import 'package:counter_clinic_web/src/route_paths.dart';
import 'package:counter_clinic_web/src/routes.dart';
import 'package:angular_components/angular_components.dart';
import 'package:http/browser_client.dart';

// AngularDart info: https://webdev.dartlang.org/angular
// Components info: https://webdev.dartlang.org/components

@Component(
  selector: 'my-app',
  styleUrls: const [
    'package:angular_components/app_layout/layout.scss.css',
    'app_component.css',
    ],
  encapsulation: ViewEncapsulation.None, // makes the style available for all components
  templateUrl: 'app_component.html',
  directives: [
    routerDirectives
  ],
  providers: [
    materialProviders, 
    ClassProvider(LoginService), 
    ClassProvider(BrowserClient), 
    ClassProvider(Location)
  ],
  exports: [RoutePaths, Routes],
)
class AppComponent {
  final String title = "Counter Clinic Web";

}
